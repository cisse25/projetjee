package com.isi.projetjee.domain.enumeration;

/**
 * The Etat enumeration.
 */
public enum Etat {
    ACTIF,
    INACTIF,
}
