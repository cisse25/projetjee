package com.isi.projetjee.service.impl;

import com.isi.projetjee.domain.Sortie;
import com.isi.projetjee.repository.SortieRepository;
import com.isi.projetjee.service.SortieService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Sortie}.
 */
@Service
@Transactional
public class SortieServiceImpl implements SortieService {
    private final Logger log = LoggerFactory.getLogger(SortieServiceImpl.class);

    private final SortieRepository sortieRepository;

    public SortieServiceImpl(SortieRepository sortieRepository) {
        this.sortieRepository = sortieRepository;
    }

    @Override
    public Sortie save(Sortie sortie) {
        log.debug("Request to save Sortie : {}", sortie);
        return sortieRepository.save(sortie);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Sortie> findAll(Pageable pageable) {
        log.debug("Request to get all Sorties");
        return sortieRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Sortie> findOne(Long id) {
        log.debug("Request to get Sortie : {}", id);
        return sortieRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Sortie : {}", id);
        sortieRepository.deleteById(id);
    }
}
