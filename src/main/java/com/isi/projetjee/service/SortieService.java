package com.isi.projetjee.service;

import com.isi.projetjee.domain.Sortie;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Sortie}.
 */
public interface SortieService {
    /**
     * Save a sortie.
     *
     * @param sortie the entity to save.
     * @return the persisted entity.
     */
    Sortie save(Sortie sortie);

    /**
     * Get all the sorties.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Sortie> findAll(Pageable pageable);

    /**
     * Get the "id" sortie.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Sortie> findOne(Long id);

    /**
     * Delete the "id" sortie.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
