/**
 * View Models used by Spring MVC REST controllers.
 */
package com.isi.projetjee.web.rest.vm;
