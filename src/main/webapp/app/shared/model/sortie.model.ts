import { Moment } from 'moment';
import { IProduit } from 'app/shared/model/produit.model';
import { IFacture } from 'app/shared/model/facture.model';

export interface ISortie {
  id?: number;
  date?: Moment;
  quantite?: number;
  produit?: IProduit;
  facture?: IFacture;
}

export class Sortie implements ISortie {
  constructor(public id?: number, public date?: Moment, public quantite?: number, public produit?: IProduit, public facture?: IFacture) {}
}
