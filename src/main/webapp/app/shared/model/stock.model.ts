import { Moment } from 'moment';
import { IProduit } from 'app/shared/model/produit.model';

export interface IStock {
  id?: number;
  quantite?: number;
  date?: Moment;
  produit?: IProduit;
}

export class Stock implements IStock {
  constructor(public id?: number, public quantite?: number, public date?: Moment, public produit?: IProduit) {}
}
