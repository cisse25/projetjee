export interface IClient {
  id?: number;
  prenom?: string;
  nom?: string;
  tel?: number;
}

export class Client implements IClient {
  constructor(public id?: number, public prenom?: string, public nom?: string, public tel?: number) {}
}
