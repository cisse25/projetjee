import { Moment } from 'moment';
import { IClient } from 'app/shared/model/client.model';
import { Etat } from 'app/shared/model/enumerations/etat.model';

export interface IFacture {
  id?: number;
  numero?: string;
  date?: Moment;
  quantite?: number;
  etat?: Etat;
  client?: IClient;
}

export class Facture implements IFacture {
  constructor(
    public id?: number,
    public numero?: string,
    public date?: Moment,
    public quantite?: number,
    public etat?: Etat,
    public client?: IClient
  ) {}
}
