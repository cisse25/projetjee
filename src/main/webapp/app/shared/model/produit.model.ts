import { ICategorie } from 'app/shared/model/categorie.model';

export interface IProduit {
  id?: number;
  libelle?: string;
  description?: string;
  prix?: number;
  categorie?: ICategorie;
}

export class Produit implements IProduit {
  constructor(
    public id?: number,
    public libelle?: string,
    public description?: string,
    public prix?: number,
    public categorie?: ICategorie
  ) {}
}
