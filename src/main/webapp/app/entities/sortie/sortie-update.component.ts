import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISortie, Sortie } from 'app/shared/model/sortie.model';
import { SortieService } from './sortie.service';
import { IProduit } from 'app/shared/model/produit.model';
import { ProduitService } from 'app/entities/produit/produit.service';
import { IFacture } from 'app/shared/model/facture.model';
import { FactureService } from 'app/entities/facture/facture.service';

type SelectableEntity = IProduit | IFacture;

@Component({
  selector: 'jhi-sortie-update',
  templateUrl: './sortie-update.component.html',
})
export class SortieUpdateComponent implements OnInit {
  isSaving = false;
  produits: IProduit[] = [];
  factures: IFacture[] = [];
  dateDp: any;

  editForm = this.fb.group({
    id: [],
    date: [],
    quantite: [],
    produit: [],
    facture: [],
  });

  constructor(
    protected sortieService: SortieService,
    protected produitService: ProduitService,
    protected factureService: FactureService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sortie }) => {
      this.updateForm(sortie);

      this.produitService.query().subscribe((res: HttpResponse<IProduit[]>) => (this.produits = res.body || []));

      this.factureService.query().subscribe((res: HttpResponse<IFacture[]>) => (this.factures = res.body || []));
    });
  }

  updateForm(sortie: ISortie): void {
    this.editForm.patchValue({
      id: sortie.id,
      date: sortie.date,
      quantite: sortie.quantite,
      produit: sortie.produit,
      facture: sortie.facture,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const sortie = this.createFromForm();
    if (sortie.id !== undefined) {
      this.subscribeToSaveResponse(this.sortieService.update(sortie));
    } else {
      this.subscribeToSaveResponse(this.sortieService.create(sortie));
    }
  }

  private createFromForm(): ISortie {
    return {
      ...new Sortie(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value,
      quantite: this.editForm.get(['quantite'])!.value,
      produit: this.editForm.get(['produit'])!.value,
      facture: this.editForm.get(['facture'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISortie>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
