import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'categorie',
        loadChildren: () => import('./categorie/categorie.module').then(m => m.ProjetjeeCategorieModule),
      },
      {
        path: 'facture',
        loadChildren: () => import('./facture/facture.module').then(m => m.ProjetjeeFactureModule),
      },
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.ProjetjeeClientModule),
      },
      {
        path: 'produit',
        loadChildren: () => import('./produit/produit.module').then(m => m.ProjetjeeProduitModule),
      },
      {
        path: 'sortie',
        loadChildren: () => import('./sortie/sortie.module').then(m => m.ProjetjeeSortieModule),
      },
      {
        path: 'stock',
        loadChildren: () => import('./stock/stock.module').then(m => m.ProjetjeeStockModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class ProjetjeeEntityModule {}
