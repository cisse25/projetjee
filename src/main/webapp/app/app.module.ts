import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { ProjetjeeSharedModule } from 'app/shared/shared.module';
import { ProjetjeeCoreModule } from 'app/core/core.module';
import { ProjetjeeAppRoutingModule } from './app-routing.module';
import { ProjetjeeHomeModule } from './home/home.module';
import { ProjetjeeEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    ProjetjeeSharedModule,
    ProjetjeeCoreModule,
    ProjetjeeHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    ProjetjeeEntityModule,
    ProjetjeeAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class ProjetjeeAppModule {}
